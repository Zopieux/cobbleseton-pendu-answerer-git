# coding: utf-8

from pypeul import IRC, Tags
from threading import Timer
from string import ascii_uppercase
import random
import re
from collections import defaultdict

import logging
logging.basicConfig(level=logging.INFO)

import yaml
with open('conf.yml') as f:
    CONF = yaml.load(f)

SERVER = tuple(CONF['server'])
PSWD = CONF['user']['pswd']
CHAN = CONF['channel']
MY_NICK = CONF['user']['nick']
BOTNICK = CONF['botnick']

LETTERS = set(ascii_uppercase)

with open('/home/zopieux/shared-data/ods5.words') as f:
    WORDS = [_ for _ in f.read().split('\n') if _]

RE_PARTIAL_START = re.compile(r'^[_a-z ]+ [a-z_]$')
RE_PARTIAL = re.compile(r'^([_a-z ]+)\s+à toi')
RE_ANSWER = re.compile(r'^([a-z]+)\s+!')
VOWELS = 'eaiou'

def delayed(t, func, *args, **kwargs):
    timer = Timer(t, func, args=args, kwargs=kwargs)
    timer.start()

def rnd_delayed(t, func, *args, **kwargs):
    tmin, tmax = t
    delayed(random.random()*(tmax-tmin) + tmin, func, *args, **kwargs)

class PenduBot(IRC):
    def __init__(self):
        super(PenduBot, self).__init__()

        self.init()

    def init(self):
        self.playing = False
        self.shutup = False
        self.wait_quidautre = False
        self.current_word = []
        self.dontplay = set()
        self.wrong_words = set()
        self.latest_answer = None

    def on_welcome(self):
        self.join(CHAN)

    def stupidenough(self):
        l = len(self.current_word)
        dunno = self.current_word.count(None)
        print("THERE ARE", l, "LETTERS FOR", dunno, "UNKNOW SO RATIO IS", dunno/l)
        return dunno / l + l / 20 < .33

    def find_possible(self):
        print("FIND POSSIBLE")
        print("CURRENT", self.current_word)
        if not self.current_word:
            return []
        print("DONT PLAY", self.dontplay)
        allok = ''.join(LETTERS - self.dontplay)
        print("ALL OK", allok)
        regexp = ''.join('[%s]' % allok if l is None else l for l in self.current_word)
        print("REGEX", regexp)
        reg = re.compile('^%s$' % regexp)
        return list(set(w for w in WORDS if reg.match(w)) - self.wrong_words)

    def think_letter(self):
        print("THINK LETTER")
        possible = self.find_possible()
        if len(possible) < 50:
            print("POSSILITIES", possible)
        if len(possible) <= 3 and self.stupidenough():
            return '%s %s' % (random.choice(possible).lower(), '!'*random.randint(1, 2))
        elif not possible:
            return random.choice(('wat.', 'dunno', 'wtf', 'euh.'))
        else:
            letters = defaultdict(lambda: 0)
            for word in possible:
                for l in word:
                    if l in self.dontplay: continue
                    letters[l] += 1
            print("LETTERS", letters)
            arr = sorted([(c, l) for l, c in letters.items()], reverse=True)
            try:
                pme = arr[0][1]
                self.dontplay.add(pme)
                return pme.lower()
            except IndexError:
                return 'sheppa'

    def on_channel_message(self, umask, channel, msg):
        if channel != CHAN:
            return

        mynick = self.myself.nick.lower() 
        say = lambda t: self.message(channel, t)
        frombot = umask.nick == BOTNICK
        msg = msg.strip().lower()

        if frombot and msg.startswith(mynick) and msg.endswith("qui d'autre ?"):
            print("OK IT SEEMS I PLAY ACTUALLY")
            self.init()
            self.wait_quidautre = True
            return

        if frombot and not self.wait_quidautre and "qui d'autre ?" in msg:
            print("OK I JOIN THE GAME LOL")
            self.init()
            self.wait_quidautre = True
            if not self.shutup:
                self.shutup = True
                def cb():
                    say(random.choice(('moi','moi','moe','moa','moi !')))
                    self.shutup = False
                rnd_delayed((0.8, 1.3), cb)
            return

        if frombot and "started !" in msg:
            print("LETS DO THE SHIT XPTDR")
            self.init()
            self.playing = True
            return

        if frombot and "bien joué" in msg and mynick in msg:
            self.init()
            self.playing = True

        if not self.playing: return

        if frombot and "et non bim" in msg and self.latest_answer is not None:
            self.wrong_words.add(self.latest_answer.upper())
            print("WRONG WORDS ARE", self.wrong_words)

        if frombot and "il vous reste" in msg and "coups" in msg:
            self.dontplay = set(msg.split('(')[1][:-1].upper())
            print("ACTUALLY DONT PLAY IS", self.dontplay)

        if not frombot and len(msg) == 1 and msg.upper() in LETTERS:
            print("I SHALL NOT PLAY", msg)
            self.dontplay.add(msg.upper())
            return

        if frombot and "thx bye" in msg or "fin de partie" in msg or "terminé" in msg:
            print("GAME IS FINISHED NIGGER")
            self.init()
            return

        pmatch = RE_PARTIAL.match(msg)
        if frombot and pmatch:
            self.current_word = [None if l == '_' else l for l in pmatch.group(1).upper().split()]
            print("I FIND CURRENT IS\n\t", self.current_word)
            poss = self.find_possible()
            if len(poss) == 0:
                def wat():
                    say(random.choice(('wat', 'gnih', 'gné?', 'gné ?', 'gné', 'wtf', 'wtf ?', 'WAT', 'WHAT?', 'what', 'euh.', '.', 'klol')))
                rnd_delayed((2, 4), wat)

            elif len(poss) == 1 and self.stupidenough():
                def winning():
                    if random.random() < 0.7:
                        say(random.choice(('hm', 'ha', 'hmm', 'hmm.', 'ha ouais', 'euh ha ui', 'ata oui', 'je sais', 'jpense savoir', 'lol ok', 'haha wait', 'hm ata', 'oh yeah je crois', 'hihi ki pr', 'alors euh ptet', 'g sé lol', 'lol ui', 'ho wait')))
                    def cb():
                        say(random.choice(poss).lower() + ' !')
                        self.shutup = False
                    rnd_delayed((1.5, 4), cb)

                if not self.shutup:
                    self.shutup = True
                    rnd_delayed((1.1, 3.5), winning)
                return

        elif frombot and RE_PARTIAL_START.match(msg):
            self.current_word = [None if l == '_' else l for l in msg.upper().split()]
            print("FROM START I SAY WORD IS", self.current_word)

        else:
            m = RE_ANSWER.match(msg)
            if not frombot and m is not None:
                self.latest_answer = m.group(1)

        if frombot and "à toi" in msg and mynick in msg:
            print("OKAY I SAY SOMETHING NOW")
            if not self.shutup:
                self.shutup = True
                def cb():
                    say(self.think_letter())
                    self.shutup = False
                rnd_delayed((1, 5), cb)
            return

if __name__ == '__main__':
    bot = PenduBot()
    bot.connect(*SERVER)
    bot.ident(MY_NICK, password=PSWD)
    bot.run()

